<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\project;

class ProjectController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        $project = project::all();
        return view('project.index' , compact('project'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('project.create');

    }



    public function store(Request $request, project $project)
    {
        $data =$request->validate(['name' => 'required']);

        $project = tap(new project($data))->save();
        return redirect()->route('project.index');
      }



    public function show(project $project)
    {
        return view('project.show' , compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(project $project)
    {
        return view('project.edit' , compact('project'));
    }


    public function copy(project $project)
    {
        return view('project.copy' , compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, project $project)
    {
      $data =$request->validate(['name' => 'required']);

      $project->name = $data['name'];
      $project->save();
      return redirect()->route('project.index');
    }


    public function destroy(project $project)
    {
        $project->delete();
        return redirect()->route('project.index');
    }
}
