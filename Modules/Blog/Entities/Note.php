<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class note extends Model
{
    protected $fillable = ['content'];

    public function Blog(){

      $this->belongsTo(category::class , 'id','category_id');
    }
}
