<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \Modules\Blog\Entities\Category as blog;

class BlogController extends Controller
{

    public function index()
    {
        return view('blog::index',[
          'data'=> blog::all()
        ]);
    }


    public function create()
    {
        return view('blog::create');
    }


    public function store(Request $request)
    {
      $data =$request->validate(['title' => 'required']);

      $blog = tap(new blog($data))->save();
      return redirect()->route('blog::index');
    }


    public function show(blog $blog)
    {
        return view('blog::show',compact('blog'));
    }


    public function edit(blog $blog)
    {
        return view('blog::edit' , compact('blog'));
    }


    public function update(Request $request ,blog $blog)
{ 
      $data = $request->validate(['title' => 'required']);
      $blog->title = $data['title'];
      $blog->save();
      return redirect()->route('blog::index');

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(blog $blog)
    {
      $blog->delete();
      return redirect()->route('blog::index');
    }

    public function copy(blog $blog)
    {
        return view('blog::copy' , compact('blog'));
    }
}
