<?php

Route::group(['middleware' => 'web', 'prefix' => 'blog', 'namespace' => 'Modules\Blog\Http\Controllers'], function()
{
    Route::get('/', 'BlogController@index')->name('blog::index');
    Route::get('/create', 'BlogController@create')->name('blog::create');
    Route::get('/{blog}', 'BlogController@show')->name('blog::show');
    Route::get('/{blog}/copy', 'BlogController@copy')->name('blog::copy');
    Route::DELETE('/{blog}', 'BlogController@destroy')->name('blog::destroy');
    Route::PUT('/{blog}', 'BlogController@update')->name('blog::update');
    Route::get('/{blog}/edit', 'BlogController@edit')->name('blog::edit');
    Route::post('/', 'BlogController@store')->name('blog::store');

});
