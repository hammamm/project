@extends('blog::layouts.master')
@section('content')

{!!Form::model($blog,['route'=> 'blog::store',$blog->id])!!}
@include('blog::form')
{!!Form::close()!!}

@endsection
