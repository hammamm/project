@extends('blog::layouts.master')

@section('content')
    <a href="{{route('blog::index')}}">go back home</a>
    <table>
      <tr>
        <th>id</th>
        <th>name</th>
      </tr>
      <tr>
        <td>{{$blog->id}}</td>
        <td>{{$blog->title}}</td>
        <td><form  action="{{route('blog::destroy',['id' => $blog->id ])}}" method="post">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
          <button >delete</button>
        </form></td>
        <td><a href="{{route('blog::edit',['id' => $blog->id])}}">Edit</a></td>
        <td><a href="{{route('blog::copy',['id' => $blog->id])}}">copy</a></td>
      </tr>
    </table>
@endsection
