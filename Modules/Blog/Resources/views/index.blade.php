@extends('blog::layouts.master')

@section('content')
    <h1>Hello World</h1>
    <a href="{{route('blog::create')}}">create new blog</a>

    @foreach($data as $row)
    <hr>
    <a href="{{route('blog::show' , ['id' => $row->id])}}">{{$row->title}}</a>

      <form  action="{{route('blog::destroy',['id' => $row->id ])}}" method="post">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <button >delete</button>
      </form>
      <a href="{{route('blog::edit',['id' => $row->id])}}">Edit</a>
      <a href="{{route('blog::copy',['id' => $row->id])}}">copy</a>


    @endforeach
@stop
