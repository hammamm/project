@extends('blog::layouts.master')
@section('content')

{!! Form::model($blog,['route' => ['blog::update',$blog->id],'method' => 'put']) !!}
 @include('blog::form')
{!! Form::close() !!}

@endsection
