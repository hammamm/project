@extends('blog::layouts.master')
@section('content')

{!! Form::open(['route' => 'blog::store']) !!}
 @include('blog::form')
{!! Form::close() !!}

@endsection
