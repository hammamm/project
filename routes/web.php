<?php

Route::resource('project' , 'ProjectController');
Route::get('project/{project}/copy','ProjectController@copy')->name('project.copy');
