<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>{{__('pages/projectIndex.title')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"> <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  </head>
  <body>
<h1>Projects</h1>
<a href="{{route('project.create')}}"><h3>create new</h3> </a>
@foreach($project as $project)
<hr>
<a href="{{route('project.show', ['id' => $project->id ])}}"><h3>{{$project->name}}</h3></a>
<form  action="{{route('project.destroy',['id' => $project->id ])}}" method="post">
  {{ csrf_field() }}
  {{ method_field('DELETE') }}
  <button >delete</button>
</form>
<a href="{{route('project.edit',['id' => $project->id])}}">Edit</a>
@endforeach

</body>
</html>
