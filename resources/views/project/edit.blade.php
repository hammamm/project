<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>{{__('pages/projectIndex.title')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"> <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  </head>
  <body>
<form class="form" action="{{route('project.update',['id'=>$project->id])}}" method="post">
  {{ csrf_field() }}
<input type="hidden" name="_method" value="PUT">
<input type="text" name="name" value="{{$project->name}}">
<button>update</button>
</form>
  </body>
</html>
