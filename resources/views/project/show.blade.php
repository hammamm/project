<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <a href="{{route('project.index')}}">go back home</a>
    <table>
      <tr>
        <th>id</th>
        <th>name</th>
      </tr>
      <tr>
        <td>{{$project->id}}</td>
        <td>{{$project->name}}</td>
        <td><form  action="{{route('project.destroy',['id' => $project->id ])}}" method="post">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
          <button >delete</button>
        </form></td>
        <td><a href="{{route('project.edit',['id' => $project->id])}}">Edit</a></td>
        <td><a href="{{route('project.copy',['id' => $project->id])}}">copy</a></td>
      </tr>
    </table>
  </body>
</html>
